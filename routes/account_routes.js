/**
 * Created by Ashlop on 5/6/17.
 */
/**
 * Created by Ashlop on 5/2/17.
 */
var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');

router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', {'result': result});
        }
    });
});

router.get('/', function(req, res){
    if(req.query.account_id == null){
        res.send('account_id is empty');
    }
    else {
        account_dal.getById(req.query.account_id, function(err, result) {
            if(err) {
                res.send(err);
            }

            else {
                res.render('/account/accountViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    account_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else{
            res.render('account/accountAdd', {'account': result});
        }
    });
});


router.get('/insert', function(req,res){
    if (req.query.first_name == null){
        res.send('First Name must be provided.');
    }
    else if (req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if(req.query.email == null){
        res.send('Email must be provided.');
    }
    else {
        account_dal.insert(req.query, function(err, result) {
            if(err){
                res.send(err);
            }
            else {
                res.redirect(302, '/account/all');
            }
        });
    }
});


router.get('/edit', function (req, res){
    if(req.query.account_id == null) {
        res.send('An account_id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {'account': result[0][0]});
        })
    }
});


router.get('/update', function(req, res){
    account_dal.update(req.query, function(err, result){
        res.redirect(302, '/account/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.account_id == null){
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err){
                res.send(err);
            }
            else{
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;