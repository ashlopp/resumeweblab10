var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
create or replace view resume_view as
select ac.first_name, ac.last_name, r.resume_name from resume r
join account ac on ac.account_id = r.account_id;

select * from resume_view;

*/

exports.getAll = function(callback) {
    var query = 'SELECT * FROM resume;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, account_id, callback) {
    var query = 'select ac.first_name, ac.last_name, r.resume_name from resume r'+
    'join account ac on ac.account_id = r.account_id'+
    'where r.account_id = ac.account_id;';
    var queryData = [resume_id, account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {
        var query = 'INSERT INTO resume(account_id, resume_name) VALUES(?, ?)';

        var queryData = [params.account_id, params.resume_name];

        connection.query(query, queryData, function (err, result) {
            callback(err, result);
        });
    };



exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var resumeAccountInsert = function(resume_id, accountIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    //might need to include resume_name
    var query = 'INSERT INTO resume (resume_id, account_id) VALUES (?,?)';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeAccountData = [];
    //change back to params.account_id
    if (accountIdArray.constructor === Array) {
        for (var i = 0; i < accountIdArray.length; i++) {
            resumeAccountData.push([resume_id, accountIdArray[i]]);
        }
    }
    else {
        resumeAccountData.push([resume_id, accountIdArray]);
    }
    connection.query(query, [resumeAccountData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeAccountInsert = resumeAccountInsert;

//declare the function so it can be used locally
var resumeAccountDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeAccountDeleteAll = resumeAccountDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete resume entries for this account
        //why does it need to call delete if we are just changing the data?
        resumeAccountDeleteAll(params.account_id, function(err, result){

            if(params.resume_id != null) {
                //insert account ids
                resumeAccountInsert(params.resume_id, params.account_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
 DELIMITER //
 create procedure resume_get_info(_resume_id int)
 begin
 select * from resume where resume_id = _resume_id;

 select ac.first_name, ac.last_name, r.resume_name from resume r
 left join account ac on ac.account_id = r.account_id
 where r.resume_id = _resume_id;

 END; //
 DELIMITER ;
 call resume_get_info(99);
 drop procedure if exists resume_get_info;
 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_get_info(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};